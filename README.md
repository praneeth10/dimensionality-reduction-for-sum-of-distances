# Dimensionality Reduction for Sum-of-Distances

This repository contains an implementation and experiments from the paper "Dimensionality Reduction for Sum-of-Distances Metric". The function `dimreduce` accepts 3 parameters `A, k, target_dimension` and performs 5 iterations of algorithm in which each iteration returns a subspace of dimension at most `target_dimension/5`. 

Tests are performed on Synthetic and Covtype Datasets as described in the paper.
